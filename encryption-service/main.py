# Python 3 server example
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse  import urlparse, parse_qs
from cryptography.fernet import Fernet
import os

hostName = "0.0.0.0"
serverPort = 8080
key = os.environ['SUPER_ENCRYPTION_KEY']
fernet = Fernet(key)

class MyServer(BaseHTTPRequestHandler):
    def do_GET(self):
        path = urlparse(self.path).path
        print(path)
        if(path == "/encrypt"):
            query_components = parse_qs(urlparse(self.path).query)
            try:
                text = query_components["text"][0]
                encryptedText = fernet.encrypt(text.encode())
                print(encryptedText)
                self.send_response(200)
                self.send_header("Content-type", "text/plain")
                self.end_headers()
                self.wfile.write(encryptedText)
            except KeyError:
                self.send_response(400)
                self.send_header("Content-type", "text/plain")
                self.end_headers()
                self.wfile.write(bytes("need to have the ?text= variable in get request","utf-8"))
        else:
            self.send_response(400)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            self.wfile.write(bytes("url not supported","utf-8"))

if __name__ == "__main__":        
    webServer = HTTPServer((hostName, serverPort), MyServer)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")