# Mcast Freshers Week RedAcre Devops

## Introduction

We have created a micro-service called **encryption-service** that we want to run on our development and production environments. It is always a challenge to make sure that all environments are exactly the same and we have decided we will use **docker** to tackle this issue.
- DevOps intern task https://gitlab.com/redacre/mcast-freshers-week-devops for https://www.mcast.edu.mt/freshers-week-event-2022-23/

# Test
The service exposes a /encrypt endpoint which encrypts text 
- Access from your browser: http://localhost:8080/encrypt?text=YOUR_STRING

# How to Run
- install python3
- Create environment variable 
    - SUPER_ENCRYPTION_KEY="ZmDfcTF7_60GrrY167zsiPd67pEvs0aGOv2oasOM1Pg="
- pip install -r requirements.txt
- python -u main.py

# Deliverables
- Create a **Dockerfile** which runs the application by following the above instructions and test the application to make sure it works
- **Bonus:** add a **docker-compose** file to help our developers run the application in an easier way
- **Readme** file how to run your docker deliverables
- Submit this to mcastfreshers@redacreltd.com or SPEAK to us at the stand for more info and perform the test on our laptops :)
